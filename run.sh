#!/bin/bash


    echo "=> Found authorized keys"
    mkdir -p /root/.ssh
    chmod 700 /root/.ssh
    mv /authorized_keys /root/.ssh/authorized_keys
    chmod 600 /root/.ssh/authorized_keys


if [ ! -f /.root_pw_set ]; then
	/set_root_pw.sh
fi
exec /usr/sbin/sshd -D

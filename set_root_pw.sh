#!/bin/bash

PASS=“zxcvbnm”
echo "root:$PASS" | chpasswd
echo "=> Setting a ${_word} password to the root user"

echo "========================================================================"
echo "You can now connect to this CentOS container via SSH using:"
echo ""
echo "    ssh -p <port> root@<host>"
echo "and enter the root password '$PASS' when prompted"
echo ""
echo "Please remember to change the above password as soon as possible!"
echo "========================================================================"

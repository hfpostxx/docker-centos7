FROM centos:centos7
MAINTAINER kkstun <kkstun@qq.com>

RUN yum -y install openssh-server openssh-clients && \
    rm -f /etc/ssh/ssh_host_ecdsa_key /etc/ssh/ssh_host_rsa_key && \
    ssh-keygen -q -N "" -t dsa -f /etc/ssh/ssh_host_ecdsa_key && \
    ssh-keygen -q -N "" -t rsa -f /etc/ssh/ssh_host_rsa_key && \
    ssh-keygen -q -N "" -t rsa -f /etc/ssh/ssh_host_ed25519_key && \
    sed -i "s/#UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config && \
    sed -i 's%#PermitRootLogin yes%PermitRootLogin yes%' /etc/ssh/sshd_config && \
    sed -i 's%#GatewayPorts no%GatewayPorts yes%' /etc/ssh/sshd_config
    sed -i 's%#Protocol 2%Protocol 2%' /etc/ssh/sshd_config

ADD set_root_pw.sh /set_root_pw.sh
ADD authorized_keys /authorized_keys
ADD run.sh /run.sh
RUN chmod +x /*.sh
ENV AUTHORIZED_KEYS **None**
EXPOSE 22
CMD ["/run.sh"]
